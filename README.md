# README
Author: Alexander Schiessl  
Date: 2015/10
Version: 0.0.6
Copyright: Alexander Schiessl, arjs.net  
Web: http://arjs.net  
License: Unlicense, http://unlicense.org  
Description:    
A simple cmdb command line tool for a poor man cmdb.

## Usage:
help:
cmdb -h

Create new cmdb folder:
cmdb -o "path to cmdb folder" -j "path to json file" create

Create new key/value:
cmdb -o "path to cmdb folder" -K key -V value create

Create new structure:
cmdb -o "path to cmdb folder" -K key -j "path to json strucrure" create

Get all:
cmdb -i "path to cmdb folder" get

Get key value:
cmdb -i "path to cmdb folder" -K key get

Get key structure:
cmdb -i "path to cmdb folder" -K "path to key" get

Put key structure:
cmdb -i "path to cmdb folder" -K key -j "path to json structure" put

Put key/value:
cmdb -i "path to cmdb folder" -K key -V value put

Dump all:
cmdb -i "path to cmdb folder" -o "path to output" -f "output file name" dump

## Intention:
Background for this program was simple, I wanted to try a new way to do a configuration.
Usually you put everything in a DB or in files. And then you find ways to version it or make it work
with more environments. Find a way to retrieve or set/change values. And I thought there are
enough solutions out there. I wanted to try the idea of unix. Everything is a file.

So I created a solution which can take a json file with key/value, key/list or key/structure and creates a
folder/file structure in the file system. So you can use normal file/folder check or methods. Or put a webserver
on top of it and access it via web interface.

## To Do:
- Maybe include a simple Flask server
- Optimize code and make it more pythonmatic
