"""
Tests for command CREATE
"""

# imports
import os
import helper
from common.log import view
from commands.create import run as create_run

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'

# some variables


def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    helper.setup_module(module)


def teardown_module(module):
    """ teardown any state that was previously setup with a setup_module
    method.
    """
    helper.teardown_module(module)


args_create_clean = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'current.json'),
    '--clean',
    'create'
]


def test_create_clean():
    """Test: Create Clean From File; Expected True"""
    view('.', 'Test: Create Clean From File; Expected True')
    helper.get_args(args_create_clean)
    assert create_run(helper.args)
    helper.assert_files('current.json')


args_create_key_in_structure = [
    '-o', helper.OUT_PATH,
    '-K', 'builds/tag',
    '-V', 'built',
    'create'
]


def test_create_key_in_structure():
    """Test: Create Key In Structure; Expected True"""
    view('.', 'Test: Create Key In Structure; Expected True')
    test_create_clean()
    helper.get_args(args_create_key_in_structure)
    assert create_run(helper.args)
    helper.assert_files('current+key.json')


args_create_existing_key_in_structure = [
    '-o', helper.OUT_PATH,
    '-K', 'builds/version',
    '-V', '1.2.0.0',
    'create'
]


def test_create_existing_key_in_structure():
    """Test: Create Existing Key In Structure; Expected False"""
    view('.', 'Test: Create Existing Key In Structure; Expected False')
    test_create_clean()
    helper.get_args(args_create_existing_key_in_structure)
    assert create_run(helper.args) == False


args_create_key_in_array = [
    '-o', helper.OUT_PATH,
    '-K', 'environments/2',
    '-j', os.path.join(helper.OUT_BASE, 'key+array.json'),
    'create'
]


def test_create_key_in_array():
    """Test: Create Key In Array; Expecting True"""
    view('.', 'Test: Create Key In Array; Expecting True')
    test_create_clean()
    helper.get_args(args_create_key_in_array)
    assert create_run(helper.args)
    helper.assert_files('current+key+array.json')


args_create_existing_key_in_array = [
    '-o', helper.OUT_PATH,
    '-K', 'environments/1',
    '-j', os.path.join(helper.OUT_BASE, 'key+array.json'),
    'create'
]


def test_create_existing_key_in_array():
    """Test: Create Existing Key In Array; Expecting False"""
    view('.', 'Test: Create Existing Key In Array; Expecting False')
    test_create_clean()
    helper.get_args(args_create_existing_key_in_array)
    assert create_run(helper.args) == False


args_create_key_append_array = [
    '-o', helper.OUT_PATH,
    '-K', 'environments',
    '-j', os.path.join(helper.OUT_BASE, 'key+array.json'),
    'create'
]


def test_create_key_append_array():
    """Test: Create Key In Array; Expected True"""
    view('.', 'Test: Create Key In Array; Expected True')
    test_create_clean()
    helper.get_args(args_create_key_append_array)
    assert create_run(helper.args)
    helper.assert_files('current+key+array.json')


args_create_array = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'array.json'),
    'create'
]


def test_create_array():
    """Test: Create Array; Expected True"""
    view('.', 'Test: Create Array; Expected True')
    test_create_clean()
    helper.get_args(args_create_array)
    assert create_run(helper.args)
    helper.assert_files('current+array.json')


args_create_array_again = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'array.json'),
    'create',
]


def test_create_array_again():
    """Test: Create Array Again; Expected False"""
    view('.', 'Test: Create Array Again; Expected False')
    test_create_array()
    helper.get_args(args_create_array_again)
    assert create_run(helper.args) == False


args_create_array_in_existing_key = [
    '-o', helper.OUT_PATH,
    '-K', 'managed',
    '-j', os.path.join(helper.OUT_BASE, 'array.json'),
    'create'
]


def test_create_array_in_existing_key():
    """Test: Create Array In Existing Key; Expected True"""
    view('.', 'Test: Create Array In Existing Key; Expected True')
    test_create_clean()
    helper.get_args(args_create_array_in_existing_key)
    assert create_run(helper.args)
    helper.assert_files('current+array+struct.json')


args_create_array_in_new_key = [
    '-o', helper.OUT_PATH,
    '-K', 'managed_new',
    '-j', os.path.join(helper.OUT_BASE, 'array.json'),
    'create'
]


def test_create_array_in_new_key():
    """Test: Create Array In New Structure; Expected True"""
    view('.', 'Test: Create Array In New Structure; Expected True')
    test_create_clean()
    helper.get_args(args_create_array_in_new_key)
    assert create_run(helper.args)
    helper.assert_files('current+array+new+struct.json')


args_create_empty_array = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'empty+array.json'),
    'create'
]


def test_create_empty_array():
    """Test: Create Empty Array; Expected True"""
    view('.', 'Test: Create Empty Array; Expected True')
    test_create_clean()
    helper.get_args(args_create_empty_array)
    assert create_run(helper.args)
    helper.assert_files('current+empty+array.json')


args_create_empty_array_again = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'empty+array.json'),
    'create'
]


def test_create_empty_array_again():
    """Test: Create Empty Array Again; Expected False"""
    view('.', 'Test: Create Empty Array Again; Expected False')
    test_create_empty_array()
    helper.get_args(args_create_empty_array_again)
    assert create_run(helper.args) == False


args_create_empty_array_in_existing_key = [
    '-o', helper.OUT_PATH,
    '-K', 'deploys',
    '-j', os.path.join(helper.OUT_BASE, 'empty+array.json'),
    'create'
]


def test_create_empty_array_in_existing_key():
    """Test: Create Empty Array In Existing Key; Expected True"""
    view('.', 'Test: Create Empty Array In Existing Key; Expected True')
    test_create_clean()
    helper.get_args(args_create_empty_array_in_existing_key)
    assert create_run(helper.args)
    helper.assert_files('current+empty+array+struct.json')


args_create_empty_array_in_new_key = [
    '-o', helper.OUT_PATH,
    '-K', 'deploys_new',
    '-j', os.path.join(helper.OUT_BASE, 'empty+array.json'),
    'create'
]


def test_create_empty_array_in_new_key():
    """Test: Create Empty Array In Existing Key; Expected True"""
    view('.', 'Test: Create Empty Array In Existing Key; Expected True')
    test_create_clean()
    helper.get_args(args_create_empty_array_in_new_key)
    assert create_run(helper.args)
    helper.assert_files('current+empty+array+new+struct.json')
