import sys
import commands
from common.log import view

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'


def test_version():
    """Test for version output"""
    for plugin in commands.__all__:
        version = sys.modules['commands.{}'.format(plugin)].version()
        view('.', 'Plugin commands.{} {}'.format(plugin, version))
        assert version is not None
