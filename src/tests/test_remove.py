"""
Tests for command REMOVE
"""

# imports
import os
import helper
from common.log import view
from common.utils import read_json_data
from commands.create import run as create_run
from commands.remove import run as remove_run

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'

# some variables


def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    helper.setup_module(module)


def teardown_module(module):
    """ teardown any state that was previously setup with a setup_module
    method.
    """
    helper.teardown_module(module)


args_create_clean = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'current+array+struct.json'),
    '--clean',
    'create',
]


def test_create_clean():
    """Test: Create Clean From File; Expected True"""
    view('.', 'Test: Create Clean From File; Expected True')
    helper.get_args(args_create_clean)
    assert create_run(helper.args)
    helper.assert_files('current+array+struct.json')


args_remove_top_level_key = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'version',
    'remove',
]


def test_remove_top_level_key():
    """Test: Remove Top Level Key; Expected 1.0.0.0"""
    view('.', 'Test: Remove Top Level Key; Expected 1.0.0.0')
    test_create_clean()
    helper.get_args(args_remove_top_level_key)
    assert remove_run(helper.args) == '1.0.0.0'
    _value_file = os.path.join(helper.args.input, helper.args.key + '.value')
    assert not os.path.exists(_value_file)


args_remove_deep_level_key = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'deploys/version',
    'remove',
]


def test_remove_deep_level_key():
    """Test: Remove Deep Level Key; Expected 1.1.0.0"""
    view('.', 'Test: Remove Deep Level Key; Expected 1.1.0.0')
    test_create_clean()
    helper.get_args(args_remove_deep_level_key)
    assert remove_run(helper.args) == '1.1.0.0'
    _value_file = os.path.join(helper.args.input, helper.args.key + '.value')
    assert not os.path.exists(_value_file)


args_remove_deep_level_key_in_struct = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/version',
    'remove',
]


def test_remove_deep_level_key_in_struct():
    """Test: Remove Deep Level Key In Struct; Expected 1.0.0.0"""
    view('.', 'Test: Remove Deep Level Key In Struct; Expected 1.0.0.0')
    test_create_clean()
    helper.get_args(args_remove_deep_level_key_in_struct)
    assert remove_run(helper.args) == '1.0.0.0'
    _value_file = os.path.join(helper.args.input, helper.args.key + '.value')
    assert not os.path.exists(_value_file)


args_remove_array = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments',
    'remove',
]


def test_remove_array():
    """Test: Remove Array; Expected [{'key': 'value0'}, {'key': 'value1'}]"""
    view('.', "Test: Remove Array; Expected [{'key': 'value0'}, {'key': 'value1'}]")
    test_create_clean()
    helper.get_args(args_remove_array)
    assert remove_run(helper.args) == [{'key': 'value0'}, {'key': 'value1'}]
    _array_file = os.path.join(helper.args.input, helper.args.key + '+')
    assert not os.path.exists(_array_file)


args_remove_array_key = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/0',
    'remove',
]


def test_remove_array_key():
    """Test: Remove Array Key; Expected [{'key': 'value1'}]"""
    view('.', "Test: Remove Array Key; Expected {'key': 'value1'}")
    test_create_clean()
    helper.get_args(args_remove_array_key)
    assert remove_run(helper.args) == {'key': 'value0'}
    _key, _ = helper.args.key.rsplit('/', 1)
    _array_path = os.path.join(helper.args.input, _key + '+')
    _no_file = os.path.join(_array_path, '+.json')
    assert os.path.exists(_no_file)
    _jd = read_json_data(_no_file)
    _no = int(_jd['length'])
    assert _no == 1


args_remove_array_missing_key = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/2',
    'remove',
]


def test_remove_array_missing_key():
    """Test: Remove Array Missing Key; Expected False"""
    view('.', "Test: Remove Array Missing Key; Expected False")
    test_create_clean()
    helper.get_args(args_remove_array_missing_key)
    assert remove_run(helper.args) == False


args_remove_array_negative_key = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/-1',
    'remove',
]


def test_remove_array_negative_key():
    """Test: Remove Array Negative Key; Expected False"""
    view('.', "Test: Remove Array Negative Key; Expected False")
    test_create_clean()
    helper.get_args(args_remove_array_negative_key)
    assert remove_run(helper.args) == False


args_remove_array_in_struct = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test',
    'remove',
]


def test_remove_array_in_struct():
    """Test: Remove Array In Struct; Expected [{'key': 'test0'}, {'key': 'test1'}]"""
    view('.', "Test: Array In Struct; Expected [{'key': 'test0'}, {'key': 'test1'}]")
    test_create_clean()
    helper.get_args(args_remove_array_in_struct)
    assert remove_run(helper.args) == [{'key': 'test0'}, {'key': 'test1'}]


args_remove_array_key_in_struct = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/0',
    'remove',
]


def test_remove_array_key_in_struct():
    """Test: Remove Array Key In Struct; Expected {'key': 'test0'}"""
    view('.', "Test: Remove Array Key In Struct; Expected {'key': 'test0'}")
    test_create_clean()
    helper.get_args(args_remove_array_key_in_struct)
    assert remove_run(helper.args) == {'key': 'test0'}


args_remove_array_missing_key_in_struct = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/2',
    'remove',
]


def test_remove_array_missing_key_in_struct():
    """Test: Remove Array Missing Key In Struct; Expected False"""
    view('.', "Test: Remove Array Missing Key In Struct; Expected False")
    test_create_clean()
    helper.get_args(args_remove_array_missing_key_in_struct)
    assert remove_run(helper.args) == False


args_remove_array_negative_key_in_struct = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/-1',
    'remove',
]


def test_remove_array_negative_key_in_struct():
    """Test: Remove Array Negative Key In Struct; Expected False"""
    view('.', "Test: Remove Array Negative Key In Struct; Expected False")
    test_create_clean()
    helper.get_args(args_remove_array_negative_key_in_struct)
    assert remove_run(helper.args) == False


args_remove_array_wrong_key = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/a',
    'remove',
]


def test_remove_array_wrong_key():
    """Test: Remove Array Wrong Key; Expected False"""
    view('.', "Test: Remove Array Wrong Key; Expected False")
    test_create_clean()
    helper.get_args(args_remove_array_wrong_key)
    assert remove_run(helper.args) == False
