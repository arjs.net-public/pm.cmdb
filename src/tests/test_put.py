"""
Tests for command PUT
"""

# imports
import os
import helper
from common.log import view
from commands.create import run as create_run
from commands.put import run as put_run

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'

# some variables


def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    helper.setup_module(module)


def teardown_module(module):
    """ teardown any state that was previously setup with a setup_module
    method.
    """
    helper.teardown_module(module)


args_create_clean = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'current+array+struct.json'),
    '--clean',
    'create',
]


def test_create_clean():
    """Test: Create Clean From File; Expected True"""
    view('.', 'Test: Create Clean From File; Expected True')
    helper.get_args(args_create_clean)
    assert create_run(helper.args)
    helper.assert_files('current+array+struct.json')


args_set_top_level = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'version',
    '-V', '1.2.0.0',
    'put',
]


def test_set_top_level():
    """Test: Set Top Level Value; Expected 1.2.0.0"""
    view('.', 'Test: Set Top Level Value; Expected 1.2.0.0')
    test_create_clean()
    helper.get_args(args_set_top_level)
    assert put_run(helper.args) == '1.2.0.0'


args_set_top_level_failed = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'failed',
    '-V', '1',
    'put',
]


def test_set_top_level_failed():
    """Test: Set Top Level Value Failed; Expected False"""
    view('.', 'Test: Set Top Level Value Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_top_level_failed)
    assert put_run(helper.args) == False


args_set_deep_level = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'builds/version',
    '-V', '1.2.0.0',
    'put',
]


def test_set_deep_level():
    """Test: Set Value In A Deep Level; Expected 1.2.0.0"""
    view('.', 'Test: Set Value In A Deep Level; Expected 1.2.0.0')
    test_create_clean()
    helper.get_args(args_set_deep_level)
    assert put_run(helper.args) == '1.2.0.0'


args_set_deep_level_failed_1 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'build/failed',
    '-V', '1'
    'put',
]


def test_set_deep_level_failed_1():
    """Test: Set Value In A Deep Level Failed; Expected False"""
    view('.', 'Test: Set Value In A Deep Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_deep_level_failed_1)
    assert put_run(helper.args) == False


args_set_deep_level_failed_2 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'builds/failed',
    '-V', '1',
    'put',
]


def test_set_deep_level_failed_2():
    """Test: Set Value In A Deep Level Failed; Expected False"""
    view('.', 'Test: Set Value In A Deep Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_deep_level_failed_2)
    assert put_run(helper.args) == False


args_set_top_level_array_value = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/1/key',
    '-V', 'value2',
    'put',
]


def test_set_top_level_array_value():
    """Test: Set Array Value On Top Level; Expected value2"""
    view('.', 'Test: Set Array Value On Top Level; Expected value2')
    test_create_clean()
    helper.get_args(args_set_top_level_array_value)
    assert put_run(helper.args) == 'value2'


args_set_top_level_array_value_failed_1 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environment/1/key',
    '-V', 'value2',
    'put',
]


def test_set_top_level_array_value_failed_1():
    """Test: Set Array Value On Top Level Failed; Expected False"""
    view('.', 'Test: Set Array Value On Top Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_top_level_array_value_failed_1)
    assert put_run(helper.args) == False


args_set_top_level_array_value_failed_2 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/2/key',
    '-V', 'value2',
    'put',
]


def test_set_top_level_array_value_failed_2():
    """Test: Set Array Value On Top Level Failed; Expected False"""
    view('.', 'Test: Set Array Value On Top Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_top_level_array_value_failed_2)
    assert put_run(helper.args) == False


args_set_top_level_array_value_failed_3 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/-1/key',
    '-V', 'value2',
    'put',
]


def test_set_top_level_array_value_failed_3():
    """Test: Set Array Value On Top Level Failed; Expected False"""
    view('.', 'Test: Set Array Value On Top Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_top_level_array_value_failed_3)
    assert put_run(helper.args) == False


args_set_top_level_array_value_failed_4 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments//key',
    '-V', 'value2',
    'put',
]


def test_set_top_level_array_value_failed_4():
    """Test: Set Array Value On Top Level Failed; Expected False"""
    view('.', 'Test: Set Array Value On Top Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_top_level_array_value_failed_4)
    assert put_run(helper.args) == False


args_set_top_level_array_value_failed_5 = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'environments/1/keys',
    '-V', 'value2',
    'put',
]


def test_set_top_level_array_value_failed_5():
    """Test: Set Array Value On Top Level Failed; Expected False"""
    view('.', 'Test: Set Array Value On Top Level Failed; Expected False')
    test_create_clean()
    helper.get_args(args_set_top_level_array_value_failed_5)
    assert put_run(helper.args) == False


args_set_deep_level_array_value = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/1/key',
    '-V', 'test2',
    'put',
]


def test_set_deep_level_array_value():
    """Test: Set Array Value At Deep Level; Expected test2"""
    view('.', 'Test: Set Array Value At Deep Level; Expected test2')
    test_create_clean()
    helper.get_args(args_set_deep_level_array_value)
    assert put_run(helper.args) == 'test2'


args_set_deep_level_array = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test',
    '-j', os.path.join(helper.OUT_BASE, 'array+changed.json'),
    'put',
]


def test_set_deep_level_array():
    """Test: Set Array At Deep Level; Expected test2"""
    view('.', 'Test: Set Array Value At Deep Level; Expected test2')
    test_create_clean()
    helper.get_args(args_set_deep_level_array)
    assert put_run(helper.args)


args_append_deep_level_array = [
    '-d',
    '-i', helper.OUT_PATH,
    '-K', 'managed/test',
    '-j', os.path.join(helper.OUT_BASE, 'empty+array.json'),
    'put',
]


def test_append_deep_level_array():
    """Test: Append Array At Deep Level; Expected True"""
    view('.', 'Test: Append Array Value At Deep Level; Expected True')
    test_create_clean()
    helper.get_args(args_append_deep_level_array)
    assert put_run(helper.args)
