"""
Helper for Tests
"""

# imports
import os
import sys
import shutil
import hashlib
try:
    _path = os.getcwd()
    for _dir in ['.', '..', 'src']:
        _check = os.path.realpath(os.path.join(_path, _dir))
        if os.path.exists(os.path.join(_check, '__main__.py')):
            sys.path.append(_check)
            config_path = _check
            import common
            from common.log import view
            break
        else:
            pass

except:
    print('Error: No import possible')
    sys.exit(0)


# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'

# some variables
OUT_BASE = 'src/tests/files'
OUT_PATH = os.path.join(OUT_BASE, 'cmdb')
DUMP_PATH = os.path.join(OUT_BASE, 'dump')
args = None
configuration = None


def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    view('.', 'setup: {}'.format(module))
    global configuration

    _config_file = os.path.realpath(os.path.join(config_path, 'default.config'))
    if _config_file and os.path.exists(_config_file):
        configuration = common.fetch_configuration(_config_file)
    else:
        raise EnvironmentError('{} ({})'.format(common.messages.MSG_E_0002, _config_file))



def teardown_module(module):
    """ teardown any state that was previously setup with a setup_module
    method.
    """
    view('.', 'teardown: {}'.format(module))
    for folder in [OUT_PATH, DUMP_PATH]:
        if os.path.exists(folder):
            shutil.rmtree(folder)


def get_args(new_args):
    """Helper to get the arguments.
    :type new_args: str[]
    """
    global args, configuration
    args = common.fetch_arguments(configuration, new_args=new_args)
    common.messages_extend(configuration.items('messages'), args.debug)
    common.check_options(args, configuration)


def assert_files(expected_content_file, debug=False):
    """takes the default _.keys file and compares md5's with a custom content file.
    :type debug: bool
    :type expected_content_file: str
    """
    file1 = common.read_json_data(os.path.join(OUT_PATH, '_.json'))
    if debug:
        view('.', 'File 1: \n{}'.format(common.dump_json_string(file1)))

    file2 = common.read_json_data(os.path.join(OUT_BASE, expected_content_file))
    if debug:
        view('.', 'File 2: \n{}'.format(common.dump_json_string(file2)))

    md5_1 = hashlib.md5()
    _str1 = common.dump_json_string(file1, byteify=True)
    md5_1.update(_str1)
    _str1 = 'MD5 #1: {}'.format(md5_1.hexdigest())
    view('.', _str1)
    md5_2 = hashlib.md5()
    _str2 = common.dump_json_string(file2, byteify=True)
    md5_2.update(_str2)
    _str2 = 'MD5 #2: {}'.format(md5_2.hexdigest())
    view('.', _str2)
    assert md5_1.hexdigest() == md5_2.hexdigest()
