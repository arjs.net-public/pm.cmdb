"""
Tests for command DUMP
"""

# imports
import os
import helper
from common.log import view
from commands.create import run as create_run
from commands.dump import run as dump_run

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'

# some variables


def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    helper.setup_module(module)


def teardown_module(module):
    """ teardown any state that was previously setup with a setup_module
    method.
    """
    helper.teardown_module(module)


args_create_clean = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'current.json'),
    '--clean',
    'create',
]


def test_create_clean():
    """Test: Create Clean From File; Expected True"""
    view('.', 'Test: Create Clean From File; Expected True')
    helper.get_args(args_create_clean)
    assert create_run(helper.args)
    helper.assert_files('current.json')


args_dump = [
    '-i', helper.OUT_PATH,
    '-o', helper.DUMP_PATH,
    'dump',
]


def test_dump():
    """Test: Dump To Standard File; Expected True"""
    view('.', 'Test: Dump To Standard File; Expected True')
    helper.get_args(args_create_clean)
    assert create_run(helper.args)
    helper.assert_files('current.json')
    helper.get_args(args_dump)
    assert dump_run(helper.args)
    helper.assert_files('dump/dump.json', debug=helper.args.debug)


args_dump_custom_file = [
    '-i', helper.OUT_PATH,
    '-o', helper.DUMP_PATH,
    '-f', 'mydump.json',
    'dump',
]


def test_dump_custom_file():
    """Test: Dump To Custom File; Expected True"""
    view('.', 'Test: Dump To Custom File; Expected True')
    test_create_clean()
    helper.get_args(args_dump_custom_file)
    assert dump_run(helper.args)
    helper.assert_files('dump/mydump.json', debug=helper.args.debug)
