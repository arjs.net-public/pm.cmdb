"""
Tests for command GET
"""

# imports
import os
import helper
from common.log import view
from commands.create import run as create_run
from commands.get import run as get_run

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'tests'

# some variables


def setup_module(module):
    """ setup any state specific to the execution of the given module."""
    helper.setup_module(module)


def teardown_module(module):
    """ teardown any state that was previously setup with a setup_module
    method.
    """
    helper.teardown_module(module)


args_create_clean = [
    '-o', helper.OUT_PATH,
    '-j', os.path.join(helper.OUT_BASE, 'current+array+struct.json'),
    '--clean',
    'create',
]


def test_create_clean():
    """Test: Create Clean From File; Expected True"""
    view('.', 'Test: Create Clean From File; Expected True')
    helper.get_args(args_create_clean)
    assert create_run(helper.args)
    helper.assert_files('current+array+struct.json')


args_get_top = [
    '-i', helper.OUT_PATH,
    'get',
]


def test_get_top():
    """Test: Get top level structure; Expected True"""
    view('.', 'Test: Get top level structure; Expected True')
    test_create_clean()
    helper.get_args(args_get_top)
    assert get_run(helper.args)
    helper.assert_files('current+array+struct.json')


args_get_version = [
    '-i', helper.OUT_PATH,
    '-K', 'version',
    'get',
]


def test_get_version():
    """Test: Get version; Expected 1.0.0.0"""
    view('.', 'Test: Get version; Expected 1.0.0.0')
    test_create_clean()
    helper.get_args(args_get_version)
    assert get_run(helper.args) == '1.0.0.0'


args_get_missing_value = [
    '-i', helper.OUT_PATH,
    '-K', 'tag',
    'get',
]


def test_get_missing_value():
    """Test: Get version; Expected False"""
    view('.', 'Test: Get version; Expected False')
    test_create_clean()
    helper.get_args(args_get_missing_value)
    print(get_run(helper.args))
    assert get_run(helper.args) == False


args_get_builds_version = [
    '-i', helper.OUT_PATH,
    '-K', 'builds/version',
    'get',
]


def test_get_builds_version():
    """Test: Get builds version; Expected 1.1.0.0"""
    view('.', 'Test: Get builds version; Expected 1.1.0.0')
    test_create_clean()
    helper.get_args(args_get_builds_version)
    assert get_run(helper.args) == '1.1.0.0'


args_get_builds_version_alternate = [
    '-i', helper.OUT_PATH,
    '-K', 'builds.version',
    'get',
]


def test_get_builds_version_alternate():
    """Test: Get builds version; Expected 1.1.0.0"""
    view('.', 'Test: Get builds version; Expected 1.1.0.0')
    test_create_clean()
    helper.get_args(args_get_builds_version_alternate)
    assert get_run(helper.args) == '1.1.0.0'


args_get_builds_missing_value = [
    '-i', helper.OUT_PATH,
    '-K', 'builds/tag',
    'get',
]


def test_get_builds_missing_value():
    """Test: Get builds version; Expected False"""
    view('.', 'Test: Get builds version; Expected False')
    test_create_clean()
    helper.get_args(args_get_builds_missing_value)
    assert get_run(helper.args) == False


args_get_array = [
    '-i', helper.OUT_PATH,
    '-K', 'environments',
    'get',
]

expected_get_array = '[{"key":"value0"},{"key":"value1"}]'


def test_get_array():
    """Test: Get array; Expected '[{"key":"value0"},{"key":"value1"}]'"""
    view('.', 'Test: Get array; Expected [{"key":"value0"},{"key":"value1"}]')
    test_create_clean()
    helper.get_args(args_get_array)
    out = ''.join(get_run(helper.args).strip().split())
    assert out == expected_get_array


args_get_array_deep = [
    '-i', helper.OUT_PATH,
    '-K', 'environments',
    'get',
]

expected_get_array_deep = '[{"key":"value0"},{"key":"value1"}]'


def test_get_array_deep():
    """Test: Get array; Expected '[{"key":"value0"},{"key":"value1"}]'"""
    view('.', 'Test: Get array; Expected [{"key":"value0"},{"key":"value1"}]')
    test_create_clean()
    helper.get_args(args_get_array_deep)
    out = ''.join(get_run(helper.args).strip().split())
    assert out == expected_get_array_deep


args_get_structure = [
    '-i', helper.OUT_PATH,
    '-K', 'managed',
    'get',
]

expected_get_structure = '{"test":[{"key":"test0"},{"key":"test1"}],"version":"1.0.0.0"}'


def test_get_structure():
    """Test: Get structure; Expected '{"test":[{"key":"test0"},{"key":"test1"}],"version":"1.0.0.0"}'"""
    view('.', 'Test: Get array; Expected {"test":[{"key":"test0"},{"key":"test1"}],"version":"1.0.0.0"}')
    test_create_clean()
    helper.get_args(args_get_structure)
    out = ''.join(get_run(helper.args).strip().split())
    assert out == expected_get_structure


args_get_array_value = [
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/1/key',
    'get',
]

expected_get_array_value = 'test1'


def test_get_array_value():
    """Test: Get structure; Expected 'test1'"""
    view('.', 'Test: Get array; Expected test1')
    test_create_clean()
    helper.get_args(args_get_array_value)
    assert get_run(helper.args) == expected_get_array_value


args_get_array_missing_value = [
    '-i', helper.OUT_PATH,
    '-K', 'managed/test/2/key',
    'get',
]

expected_get_array_missing_value = None


def test_get_array_missing_value():
    """Test: Get structure; Expected 'None'"""
    view('.', 'Test: Get array; Expected None')
    test_create_clean()
    helper.get_args(args_get_array_missing_value)
    assert get_run(helper.args) == expected_get_array_missing_value
