"""
Command: Create
Create folder/file structure from a json structure; Handles all the -j parameters in combination with -K;
Also handles the create of -K -V parameter
"""

# imports
import os
import common
from common.dotdict import DotDict
from common.log import view
from common.messages import MESSAGES

# some constants
__author__ = 'alexrjs'
__version__ = '1.1.0.0'
__suite__ = 'commands'

# some variables


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    if args.debug:
        view('', args)

    # Has -j parameter
    if args.json:
        # handle key, in case it is given, else default it
        if args.key is not None:
            key = args.key
            if '/' in key:
                key = key.replace('/', '.')

            if '.' in key:
                new_key, last = key.split('.', -1)
            else:
                new_key = key
                last = -1
        else:
            key = None
            new_key = None
            last = -1

        # Prepare out_path where the cmdb is located
        out_path = os.path.realpath(args.output if not args.output.startswith('~') else os.path.expanduser(args.output))
        if args.debug:
            view('.', 'output path: {}'.format(out_path))

        # prepare _.json file to read
        json_file = os.path.join(out_path, '_.json')
        if args.debug:
            view('.', '_.json file: {}'.format(json_file))

        # prepare _.keys file to read
        keys_file = os.path.join(out_path, '_.keys')
        if args.debug:
            view('.', '_.keys file: {}'.format(keys_file))

        # if a clean slate is wanted, delete and recreate an empty folder
        if args.clean and key is None:
            common.path_recreate(out_path)

        # if .json file exists, use it; Else empty dictionary
        if common.path_exists(json_file, exception=False):
            current_dict = DotDict(common.read_json_data(json_file))
        else:
            current_dict = DotDict()

        if args.debug:
            common.pprint(current_dict)

        # Now read the input
        infile = os.path.realpath(args.json if not args.json.startswith('~') else os.path.expanduser(args.json))
        actual_dict = DotDict(common.read_json_data(infile))
        if args.debug:
            common.pprint(actual_dict)

        # if key is present, handle the possibilities
        if key in current_dict:
            # if it is a list simply append
            if isinstance(current_dict[new_key], list):
                current_dict[new_key].append(actual_dict)

            # if it is a dict the handle new ones and ignore existing ones, with a message and a stop
            elif isinstance(current_dict[new_key], dict) or isinstance(current_dict[new_key], DotDict):
                for k in list(actual_dict.keys()):
                    if k not in current_dict[new_key]:
                        temporary_dict = {k: actual_dict[k]}
                        current_dict[new_key].update(temporary_dict)
                    else:
                        if not args.silent:
                            view('.', 'Ignored, since key already exists! ({})'.format(args.key))

                        return False

            # Ignore existing keys with a message and a stop
            else:
                if not args.silent:
                    view('.', 'Ignored, since key already exists! ({})'.format(args.key))

                return False

        # A new key to handle
        else:
            # if it is a list then look for the index, react appropriately
            if new_key in current_dict:
                if isinstance(current_dict[new_key], list):
                    if len(current_dict[new_key]) == int(last):
                        current_dict[new_key].append(actual_dict)
                    else:
                        if not args.silent:
                            view('.', 'Ignored, since index not next one!')

                        return False

            # else handle the other possibilities
            else:
                if key is not None:
                    current_dict[key] = actual_dict
                else:
                    # In case of an empty current dictionary simply make the actual_dict to it
                    if not bool(current_dict):
                        current_dict = actual_dict
                    else:
                        # Add new keys as long as they not present; Otherwise react with a message and a stop
                        for k in list(actual_dict.keys()):
                            if k not in current_dict:
                                temporary_dict = {k: actual_dict[k]}
                                current_dict.update(temporary_dict)
                            else:
                                if not args.silent:
                                    view('.', 'Ignored, since key already exists! ({})'.format(args.key))

                                return False

        if args.debug:
            common.pprint(current_dict)

        # Check again if out path exists, if not create it
        if not common.path_exists(out_path, exception=False):
            common.path_create(out_path)

        # Now it is time to write the .json file and create the file structure
        common.utils.write_json_data(json_file, current_dict, sort=False)
        common.utils.write_text_data(keys_file, '\n'.join(sorted(current_dict.keys())))
        create(out_path, current_dict, verbose=args.verbose, debug=args.debug)

        return True

    # has -K and -V
    elif args.key and args.value:
        key = args.key
        if '/' in key:
            key = key.replace('/', '.')

        out_path = args.output if not args.output.startswith('~') else os.path.expanduser(args.output)

        # Check again if out path exists, if not create it
        if not common.path_exists(out_path, exception=False):
            common.path_create(out_path)

        # if _.keys file exists, use it; Else empty dictionary
        json_file = os.path.join(out_path, '_.json')
        keys_file = os.path.join(out_path, '_.keys')
        if common.path_exists(json_file, exception=False):
            current_dict = DotDict(common.read_json_data(json_file))
        else:
            current_dict = DotDict()

        if args.debug:
            common.pprint(current_dict)

        # Handle new key
        if key not in current_dict:
            if not args.silent:
                view('.', 'Set, key/value. ({}={})'.format(args.key, args.value))

            current_dict[key] = args.value

            common.utils.write_json_data(json_file, current_dict, sort=False)
            common.utils.write_text_data(keys_file, '\n'.join(sorted(current_dict.keys())))
            create(out_path, current_dict, verbose=args.verbose, debug=args.debug)

            return True

        # Else ignore existing key with a message and a stop
        else:
            if not args.silent:
                view('.', 'Ignored, since key/value already exists! ({})'.format(args.key))

            return False

    # else unknown combination, raise error
    else:
        raise RuntimeError('{}'.format(MESSAGES['CMSG_002']))


def create(base, jd, verbose=False, debug=False):
    """Creates the files and folder from a given json structure
    :type debug: bool
    :type verbose: bool
    :type jd: dict or list
    :type base: str
    """
    if not os.path.exists(base):
        common.path_create(base)

    # write the _.keys, _.json files for the folder
    keys_file = os.path.join(base, '_.keys')
    json_file = os.path.join(base, '_.json')
    common.utils.write_json_data(json_file, jd, sort=False)

    # handle the different cases
    if isinstance(jd, list):
        _keys = []
        # Foreach element create its own folder
        for i, e in enumerate(jd):
            _keys.append(str(i))
            if isinstance(e, str):
                if verbose or debug:
                    view('.', 'Create item file: "{}/{}"'.format(base, i))

                common.write_text_data(os.path.join(base, str(i)), e)
            else:
                if verbose or debug:
                    view('.', 'Create item folder: "{}/{}"'.format(base, i))

                create(os.path.join(base, str(i)), e, verbose=verbose, debug=debug)

        common.utils.write_text_data(keys_file, '\n'.join(_keys))
        common.write_json_data(os.path.join(base, '+.json'), { 'length': str(len(jd)) } )

    elif isinstance(jd, dict):
        _keys = []
        # Go through all keys and create folder or file
        for k, v in jd.items():
            # Dictionary is a simple folder
            if isinstance(v, dict):
                if verbose or debug:
                    view('.', 'Create folder: "{}/{}"'.format(base, k))

                _keys.append(k + '/')
                current_path = os.path.join(base, k)
                create(current_path, v, verbose=verbose, debug=debug)

            # Mark lists with the + as marker; also write a file '#' with the length
            elif isinstance(v, list):
                if verbose or debug:
                    view('.', 'Create array folder: "{}/{}"'.format(base, k))

                _keys.append(k + '*')
                current_path = os.path.join(base, '{}+'.format(k))
                create(current_path, v, verbose=verbose, debug=debug)

            # Key/Value pair is a file with name "key" and a content of "value"
            elif isinstance(v, str) or isinstance(v, int) or isinstance(v, float):
                if verbose or debug:
                    view('.', 'Create file "{}/{}" with content "{}"'.format(base, k, v))

                _keys.append(k)
                current_path = os.path.join(base, '{}.value'.format(k))
                common.write_text_data(current_path, str(v))

            # Otherwise it is not a clean json structure, so raise an error
            else:
                raise ValueError('{} ({},{})'.format(common.MESSAGES['CMSG_001'], k, type(v)))

        common.utils.write_text_data(keys_file, '\n'.join(sorted(_keys)))
