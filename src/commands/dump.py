"""
Command: Dump
Dumps the cmdb in a stanard or with -f to a custom file.
"""
# imports
import os
import json
import common
from common.log import view

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'commands'

# some variables


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    if args.debug:
        view('', args)

    return dump(args)


def dump(args):
    """Goes through the files/folders and creates a json file;
    :type args: object[]
    """
    if args.debug:
        view('.', 'Input: {}'.format(args.input))
        view('.', 'Output Path: {}'.format(args.output))
        view('.', 'Output: {}'.format(args.outfile))

    try:
        out = path_to_dict(os.path.expanduser(args.input))
        if args.debug:
            view('.', '{}'.format(common.dump_json_string(out)))
    except Exception as _e:
        print(_e)

    # Prepare out_path where the cmdb is located
    out_path = args.output if not args.output.startswith('~') else os.path.expanduser(args.output)
    if args.debug:
        view('.', 'output path: {}'.format(out_path))

    # Check again if out path exists, if not create it
    if not common.path_exists(out_path, exception=False):
        common.path_create(out_path)

    outfile = os.path.join(args.output, args.outfile)
    common.write_json_data(outfile, out)

    return True


def path_to_dict(path, is_array=False, debug=False):
    """Helper function to create the json structure from a folder structure
    :type debug: bool
    :type is_array: bool
    :type path: str
    """
    jd = [] if is_array else {}

    for element in os.listdir(path):
        if element in ['_.keys', '_.json', '+.json']:
            continue

        full_path = os.path.join(path, element)
        if debug:
            view('.', 'Path: {}'.format(path))
            view('.', 'Key: {}'.format(element))
            view('.', 'Full Path: {}'.format(full_path))
            view('.', 'Is array: {}'.format(is_array))

        if os.path.isdir(full_path):
            if element.endswith('+'):
                _key = element[:-1]
                jd[_key] = path_to_dict(full_path, is_array=True, debug=debug)
                # jd[_key] = sorted(jd[_key], key=lambda k: k[0], reverse=False)
            elif is_array:
                jd.append(path_to_dict(full_path, debug=debug))
            else:
                jd[element] = path_to_dict(full_path, debug=debug)
        else:
            _key = element.replace('.value', '')
            with open(full_path, 'r') as f:
                if full_path.endswith('.json'):
                    jd[_key] = json.load(f)
                else:
                    values = f.readlines()
                    # values = [s.decode() for s in values]
                    _str = ''.join(values)
                    jd[_key] = _str

    return jd
