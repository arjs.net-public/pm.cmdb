from glob import glob
from keyword import iskeyword
from sys import path
from os.path import dirname, join, split, splitext

# some constants
__author__ = 'alexrjs'

# some variables
basedir = dirname(__file__)

__all__ = []
for name in glob(join(basedir, '*.py')):
    module = splitext(split(name)[-1])[0]
    if not module.startswith('_') and not iskeyword(module):
        try:
            __import__(__name__ + '.' + module)
        except Exception as e:
            import logging
            ch = logging.StreamHandler()
            ch.setLevel(logging.ERROR)
            logger = logging.getLogger(__name__)
            # logger.addHandler(ch)
            logger.error('ERROR: {} - %r'.format(e.msg if e.msg else str(e)), module)
            logger.warning('Warning: Ignoring exception while loading the %r plug-in.', module)
        else:
            __all__.append(module)

__all__.sort()
