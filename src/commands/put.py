# imports
import os
import common
from common.log import view
from commands.create import create

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'commands'

# some variables


def version():
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    if args.debug:
        view('', args)

    if args.debug:
        view('.', 'Input: {}'.format(args.input))
        view('.', 'Key Path: {}'.format(args.key))
        view('.', 'Value: {}'.format(args.value))

    if not args.key:
        key_path = args.input
        key_path_array = ''
    else:
        if str(args.key).endswith('+'):
            key_path = args.key[:-1]
            key_path_array = args.key
        else:
            key_path = args.key
            key_path_array = '{}+'.format(args.key)

    if args.value:
        value = args.value
    else:
        value = ''

    has_json = False
    if args.json:
        # Now read the input
        infile = args.json if not args.json.startswith('~') else os.path.expanduser(args.json)
        has_json = common.read_json_data(infile)
        if args.debug:
            common.pprint(has_json)

    cmd_create = True if 'create' == args.command else False

    jd = common.read_json_data(os.path.join(args.input, '_.json'))

    if args.debug:
        view('.', 'Key Path: {}'.format(key_path))
        view('.', 'Key Path Array: {}'.format(key_path_array))
        view('.', 'Value: {}'.format(value))
        view('.', 'Create: {}'.format(cmd_create))

    out = _put(jd, key_path, args.value, has_json, cmd_create=cmd_create, silent=args.silent, debug=args.debug)
    if args.debug:
        if isinstance(out, dict):
            view('.', 'Output: {}'.format(common.dump_json_string(out)))
        else:
            view('.', 'Output: {}'.format(out))

    out_path = os.path.expanduser(args.input)
    if args.clean:
        common.path_recreate(out_path)

    common.utils.write_json_data(os.path.join(out_path, '_.json'), jd, sort=False)
    create(out_path, jd, verbose=args.verbose, debug=args.debug)

    return out if not args.silent else ''


def _put(jd, key_path, has_value, has_json, cmd_create=False, silent=False, debug=False):
    """Handles the put of values, list and dicts"""

    # Do some tests and react accordingly
    if key_path is None and not cmd_create:
        if not silent:
            view('.', 'Ignored, since a empty key path was given!')

        return jd

    # if a key path is given and create then join or ignore
    elif key_path is not None and cmd_create:
        if key_path != '':
            if not silent:
                view('.', 'Update empty structure!')

            jd.update(has_json)

            return jd
        else:
            if not silent:
                view('.', 'Ignored Empty Key Path!')

            return False

    elif key_path is None and not cmd_create:
        if not silent:
            view('.', 'Ignored, since a empty key path was given!')

        return jd

    elements = []
    if key_path is not None:
        elements = str(key_path).split('/')

    elements = [e for e in elements if e]
    if debug:
        view('.', 'Elements {}'.format(elements))

    # Loop over the elements and decide what to to
    for idx, element in enumerate(elements):
        if debug:
            view('.', 'Element {}/{}'.format(idx, element))

        if not element:
            continue

        if element in jd:
            if isinstance(jd[element], list):
                if debug:
                    view('.', 'Delegate to list element "{}"'.format('/'.join(elements[1:]), jd[element]))

                out = handle_list(
                        jd, elements, element, idx, has_value=has_value, has_json=has_json,
                        cmd_create=cmd_create, silent=silent, debug=debug
                )
                if debug:
                    view('.', 'Output from delegate: {}'.format(out))

                if out:
                    return out
                else:
                    elements[1] = None
                    continue

            elif isinstance(jd[element], dict):
                if len(elements) >= 2:
                    if debug:
                        view('.', 'Delegate to {}'.format('/'.join(elements[1:])))

                    out = _put(
                        jd[element], '/'.join(elements[1:]), has_value, has_json, cmd_create=cmd_create, debug=debug
                    )
                    if debug:
                        view('.', 'Output from delegate: {}'.format(out))

                    if out:
                        return out
                    else:
                        elements[1] = None
                        continue
                else:
                    jd[element].update(has_json)
                    return jd

            elif isinstance(jd[element], str):
                if not silent:
                    view('.', 'Set {}={}'.format(element, has_value))

                jd[element] = has_value
                return jd[element]

            else:
                if not silent:
                    view('.', 'Ignored, since a unknown type was given!')

                break
        else:
            if cmd_create:
                if not silent:
                    view('.', 'Create {}={}'.format(element, has_value))

                jd[element] = has_value
                return jd
            else:
                if not silent:
                    view('.', 'Ignored, since element "{}" was not found! (1)'.format(element))

                return False
    else:
        if has_json:
            for _key in list(has_json.keys()):
                if _key not in jd:
                    jd.update(has_json)
                else:
                    if not silent:
                        view('.', 'Ignored, since element was found! ({})'.format(_key))

                    return False
        else:
            if not silent:
                view('.', 'Ignored, since elements {}" was not found! (2)'.format(elements))

            return False

    return jd


def handle_list(
    jd, elements, element, idx, has_value=None, has_json=False, cmd_create=False, silent=False, debug=False
):
    l = len(elements)
    if debug:
        view('.', 'Elements: {} / {}'.format(elements, l))

    if l <= idx + 1:
        if not has_json:
            if not silent:
                view('.', 'Ignored, since no json file was given!')

            return False
        else:
            if cmd_create:
                jd[element].append(has_json)
            else:
                jd[element] = has_json

            if debug:
                view('.', 'Set array: {} / {}'.format(jd[element], has_json))

            return True

    if not elements[idx + 1]:
        if not silent:
            view('.', 'Ignored, since no key was given!')

        return False

    try:
        i = int(elements[idx + 1])
        if i < 0:
            if not silent:
                view('.', 'Ignored, since a negative index was given!')

            return False
    except ValueError:
        if not silent:
            view('.', 'Ignored, since a invalid index was given!')

        return False

    l = len(jd[element])
    elements.remove(str(i))
    if debug:
        view('.', 'Cleaned Elements: {} / {} / {}'.format(elements, l, i))

    if i <= l - 1:
        if isinstance(jd[element][i], dict):
            # print str(jd[element][i]), len(elements), type(jd[element][i])
            return _put(jd[element][i], '/'.join(elements[1:]), has_value, has_json, cmd_create=cmd_create, debug=debug)
        else:
            if not silent:
                view('.', 'Ignored, since it is not a valid structure!')

            return False
    else:
        # Still not clear if this is needed
        if has_json:
            l = len(jd[element])
            if i == l:
                jd[element].append(has_json)
            else:
                print("elements mod", elements)
                print("Not Found1", i)
                if not silent:
                    view('.', 'Ignored, since index not next one!')

                return False
        else:
            print("elements mod", elements)
            print("Not Found2", i)
            if not silent:
                view('.', 'Ignored, since no json file was given!')

            return False
