# imports
import os
import common
from common.log import view

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'commands'

# some variables


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    if args.debug:
        view('', args)

    return get(args)


def get(args):
    """Handles the get requests"""
    if args.debug:
        view('.', 'Input: {}'.format(args.input))
        view('.', 'Key: {}'.format(args.key))

    if args.key is None:
        get_path = args.input
    else:
        get_path = ''
        key_path = args.key.replace('.', '/')
        if '/' in key_path:
            key, rest = key_path.split('/', 1)
            array_input = os.path.join(args.input, '{}+'.format(key))
            args.input = os.path.join(args.input, key)
            args.key = rest
            if os.path.exists(args.input):
                return get(args)
            elif os.path.exists(array_input):
                args.input = array_input
                args.key = rest
                return get(args)
        else:
            key_path = os.path.join(args.input, args.key)
            key_path_value = os.path.join(args.input, args.key + '.value')
            key_path_array = os.path.join(args.input, '{}+'.format(args.key))

            if os.path.exists(key_path):
                if args.debug:
                    view('.', 'Found: {}'.format(key_path))

                get_path = key_path

            elif os.path.exists(key_path_value):
                if args.debug:
                    view('.', 'Found: {}'.format(key_path))

                get_path = key_path_value

            elif os.path.exists(key_path_array):
                if args.debug:
                    view('.', 'Found Array: {}'.format(key_path))

                get_path = key_path_array

            else:
                if args.debug or not args.silent:
                    view('.', 'Not Found (1): {}'.format(key_path))

                return False

    # At last get the content from the folder
    if os.path.isdir(get_path):
        json_path = os.path.join(get_path, '_.json')
        if os.path.exists(json_path):
            return common.dump_json_string(common.read_json_data(json_path))
        else:
            if not args.silent:
                view('.', 'Not Found (2): {}'.format(get_path))

    # or of the file
    elif os.path.isfile(get_path):
        if args.debug:
            view('.', 'Content: {}'.format('\n'.join(common.read_text_data(get_path))))

        return '\n'.join(common.read_text_data(get_path))
