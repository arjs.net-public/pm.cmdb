# imports
import os
import shutil
import common
from common.log import view
from commands.create import create
from common.dotdict import DotDict

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'commands'

# some variables


def version():
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    if args.debug:
        view('', args)

    if args.debug:
        view('.', 'Input: {}'.format(args.input))
        view('.', 'Given Key Path: {}'.format(args.key))

    if args.key is None:
        key_path = args.input
    else:
        if str(args.key).endswith('+'):
            key_path = args.key[:-1]
        else:
            key_path = args.key

    _file_path = key_path
    if '/' in key_path:
        key_path = key_path.replace('/', '.')

    jd = DotDict(common.read_json_data(os.path.join(args.input, '_.json')))

    if args.debug:
        view('.', 'Key Path: {}'.format(key_path))

    out = None
    if key_path in jd:
        out = jd.get(key_path)
        jd.__delitem__(key_path)
        _dict_file = os.path.join(args.input, _file_path)
        _array_path = os.path.join(args.input, _file_path + '+')
        _value_file = os.path.join(args.input, _file_path + '.value')
        if os.path.exists(_value_file) and os.path.isfile(_value_file):
            os.remove(_value_file)
        elif os.path.exists(_array_path) and os.path.isdir(_array_path):
            shutil.rmtree(_array_path)
        elif os.path.exists(_dict_file) and os.path.isdir(_dict_file):
            shutil.rmtree(_dict_file)

    else:
        key, rest = key_path.rsplit('.', 1)
        if args.debug:
            view('.', 'Shortened Key Path: {} / {}'.format(key, rest))

        if key in jd:
            out = jd.get(key)
            if isinstance(out, list):
                try:
                    i = int(rest)
                    if i < 0:
                        return False

                    if i >= len(out):
                        return False

                    _ret = out.pop(i)
                except ValueError:
                    return False
    
                _array_path = os.path.join(args.input, key.replace('.', '/') + '+')
                if os.path.exists(_array_path) and os.path.isdir(_array_path):
                    shutil.rmtree(_array_path)
                    out = _ret

    if args.debug:
        view('.', 'Rest: {}'.format(common.dump_json_string(jd)))
        if isinstance(out, dict):
            view('.', 'Output: {}'.format(common.dump_json_string(out)))
        else:
            view('.', 'Output: {}'.format(out))

    out_path = args.input if not args.input.startswith('~') else os.path.expanduser(args.input)
    common.utils.write_json_data(os.path.join(out_path, '_.json'), jd, sort=False)
    create(out_path, jd, verbose=args.verbose, debug=args.debug)

    return out if not args.silent else ''
