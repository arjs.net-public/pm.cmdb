[common]
prg_name = [../config/name.txt]
prg_version = [../config/version.txt]

[options]
verbose=True
debug=True
config=True
input=True
output=True
work=False
json=True
clean=True
outfile=True
key=True
value=True
delete=True
silent=True
# default: cy = current, ly = last, ny = next, 4-digit year

[option:verbose]
short=-v
long=--verbose
type=bool
required=false
default=false
description=Optional verbose output

[option:debug]
short=-d
long=--debug
type=bool
required=false
default=false
description=Optional debug output

[option:config]
short=-c
long=--config
required=false
default=false
description=Optional configuration file; default: 'common.config'

[option:input]
short=-i
long=--input
required=false
default=data
description=Optional output path; default: 'data'

[option:output]
short=-o
long=--output
required=false
default=out
description=Optional output path; default: 'out'

[option:work]
short=-w
long=--work
required=false
default=.
description=Optional work path; default: '.'

[option:json]
short=-j
long=--json
required=false
description=Json input file

[option:clean]
long=--clean
type=bool
required=false
default=false
description=Optional clean before create

[option:outfile]
short=-f
long=--outfile
required=false
default=dump.json
description=Optional name of the dump file

[option:command]
long=command
required=true
default=version
description=Required command to execute; default: 'version'

[option:key]
short=-K
long=--key
required=false
description=Optional key path for get or put

[option:value]
short=-V
long=--value
required=false
description=Optional value for key path for put; Empty value would generate empty value

[option:delete]
long=--delete
type=bool
required=false
description=Optional delete flag for key path for put; This would delete the key path

[option:silent]
long=--silent
type=bool
required=false
description=Optional silent flag for key path for get and put; This would hide missing or overwritten key paths

[actions]
# available: [file|path]_exists, [file|path]_[re]create, check_year, extra_config|config_name|section_name,
#   check_command,version
# '*' behind name hides output
command=check_command
version=print_version
work=path_create
json=file_exists

# not used: config=file_exists
# not used: input=path_exists
# not used: output=path_create
# not used: year=check_year
# not used: user=extra_config:config:login
# not used: passwd*=extra_config:config:login

[messages]
CMSG_001=Unknown type; Cannot create file or folder;
CMSG_002=Not supported create combination;
