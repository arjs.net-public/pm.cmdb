# -*- coding: utf-8 -*-
""" utils module """
import io
import os
import sys
import json
import argparse
import fnmatch
import configparser
import yaml
from docopt import docopt, DocoptExit
from . import log
from . import shared
from . import messages

# some constants
__author__ = 'alexrjs'

# some variables
parent_parser = None

def _exit(code_):
    sys.exit(code_)

def run(args, configuration):
    """ run """
    if args.verbose:
        log.view(
            '-',
            '{} - Version {}'.format(
                get_content(configuration.get('common', 'prg_name')).strip(),
                get_content(configuration.get('common', 'prg_version')).strip()
            )
        )

    pi_name = args.command
    import commands
    if pi_name:
        plugin = getattr(commands, pi_name)
    else:
        parent_parser.print_usage()
        _exit(1)

    try:
        # see if the plugin has a 'register' attribute
        plugin_action = plugin.run
    except AttributeError as e:
        # raise an exception, log a message,
        # or just ignore the problem
        print('Exception1: %s' % e)
        _exit(1)
    else:
        # try to call it, without catching any errors
        if args.verbose:
            log.view('-', plugin.version())

        try:
            _ret = plugin_action(args)
            _exit(0)

        except Exception as e:
            print('Exception2: %s' % e)
            _exit(1)


def main(args):
    """ main """
    if args.debug or args.verbose:
        log.view('.' if args.verbose else '>', 'mode: {}'.format(args.actions))

    if not args.actions:
        raise RuntimeError(messages.MSG_E_0005)

    if shared.register[args.actions]:
        f = shared.register[args.actions]
        if not f:
            raise RuntimeError(messages.MSG_E_0007)

        f(args)
    else:
        raise RuntimeError(messages.MSG_E_0006)

    return 0


def get_content(filename):
    """ get content """
    _fn = filename
    if _fn.startswith('[') or _fn.startswith('('):
        _fn = filename[1:-1]

    if os.path.exists(_fn):
        return ''.join(read_text_data(_fn))
    elif os.path.exists(os.path.basename(_fn)):
        return ''.join(read_text_data(os.path.basename(_fn)))
    else:
        _cd = os.path.abspath(os.curdir)
        if os.path.exists(_cd):
            _fn = os.path.basename(_fn)
            _pn = os.path.join(_cd, _fn)
            if os.path.exists(_pn):
                return ''.join(read_text_data(_pn))

            _pn = os.path.join(_cd, 'config')
            _pn = os.path.join(_pn, _fn)
            if os.path.exists(_pn):
                return ''.join(read_text_data(_pn))

    return filename


def json_byteify(data):
    """ json byteify """
    if isinstance(data, dict):
        return {json_byteify(key): json_byteify(value) for key, value in data.items()}
    elif isinstance(data, list):
        return [json_byteify(element) for element in data]
    elif isinstance(data, str):
        return data.encode('utf-8')
    else:
        return data


def read_json_data(filename, byteify=False):
    """ read json data """
    if not filename:
        raise AttributeError('{} ({})'.format(messages.MSG_E_0001, filename))

    if not os.path.exists(filename):
        raise IOError('{} ({})'.format(messages.MSG_E_0002, filename))

    if not os.path.isfile(filename):
        raise IOError('{} ({})'.format(messages.MSG_E_0003, filename))

    with open(filename) as json_file:
        json_data = json.load(json_file, encoding='UTF-8')

    if byteify:
        return json_byteify(json_data)
    else:
        return json_data


def write_json_data(filename, data, indent=4, sort=True, byteify=False, debug=False):
    """ write json data """
    if not filename:
        raise AttributeError('{} ({})'.format(messages.MSG_E_0001, filename))

    if debug:
        log.view('>', '[Debug] Filename: {}'.format(filename))
        log.view('>', '[Debug] Data: {}'.format(data))

    # if os.path.exists(filename):
    #     raise IOError('{} ({})'.format(shared.MSG_E_0008, filename))

    with open(filename, 'w') as json_file:
        json.dump(
            data if not byteify else json_byteify(data),
            json_file,
            sort_keys=sort,
            indent=indent,
            separators=(', ', ' : ')
        )

def output_json(args, **kwargs):
    """ output json """
    if args.debug:
        print('func: {}'.format(output_json.__name__))

    if 'name' not in kwargs or not kwargs['name']:
        raise AttributeError('{} - {}'.format(messages.MSG_E_0001, 'name='))

    if 'data' not in kwargs or not kwargs['data']:
        raise AttributeError('{} - {}'.format(messages.MSG_E_0001, 'data='))

    if 'subname' in kwargs and kwargs['subname']:
        _fn = os.path.join(args.output, '{}.{}.json'.format(kwargs['name'], kwargs['subname']))
    else:
        _fn = os.path.join(args.output, '{}.json'.format(kwargs['name']))

    if not _fn.endswith('.json'):
        _fn = '{}.json'.format(_fn)

    if args.debug:
        log.view('>', 'Filename used: {}'.format(_fn))

    with io.open(_fn, 'w', encoding='utf-8') as f:
        f.write(str(json.dumps(kwargs['data'], ensure_ascii=True, indent=4, sort_keys=True)))

    if args.debug:
        if 'title' in kwargs and kwargs['title']:
            log.view('>', 'Title: {}'.format(kwargs['title']))

        for k in kwargs['data']:
            log.view('>', '{} -> {}'.format(k, kwargs['data'][k]))

        log.view('>', 'total: {}'.format(len(list(kwargs['data'].keys()))))


def read_text_data(filename):
    """ read text data """
    if not filename:
        raise AttributeError('{} ({})'.format(messages.MSG_E_0001, filename))

    if not os.path.exists(filename):
        raise IOError('{} ({})'.format(messages.MSG_E_0002, filename))

    if not os.path.isfile(filename):
        raise IOError('{} ({})'.format(messages.MSG_E_0003, filename))

    with open(filename) as text_file:
        text_data = text_file.readlines()

    return text_data


def write_text_data(filename, data, debug=False):
    """ write text data """
    if not filename:
        raise AttributeError('{} ({})'.format(messages.MSG_E_0001, filename))

    if debug:
        log.view('>', '[Debug] Filename: {}'.format(filename))
        log.view('>', '[Debug] Data: {}'.format(data))

    # if os.path.exists(filename):
    #     raise IOError('{} ({})'.format(shared.MSG_E_0008, filename))

    with open(filename, 'w') as text_file:
        text_file.write(data)


def read_yaml_data(filename):
    """ read yaml data """
    if not filename:
        raise AttributeError('{} ({})'.format(messages.MSG_E_0001, filename))

    if not os.path.exists(filename):
        raise IOError('{} ({})'.format(messages.MSG_E_0002, filename))

    if not os.path.isfile(filename):
        raise IOError('{} ({})'.format(messages.MSG_E_0003, filename))

    with open(filename) as yaml_file:
        yaml_data = yaml.load(yaml_file)

    return yaml_data


def fetch_configuration(filename='default.config'):
    """ fetch configuration """
    _config_file = None
    if filename and os.path.exists(filename):
        _config_file = filename

    if _config_file and not os.path.exists(_config_file):
        raise EnvironmentError('{} ({})'.format(messages.MSG_E_0002, filename))

    _configuration = configparser.ConfigParser()
    _configuration.read(_config_file)

    return _configuration


def fetch_arguments(configuration, name=None, new_args=None):
    """ fetch arguments """
    if not configuration:
        raise AttributeError('{} - {}'.format(messages.MSG_E_0001, 'configuration'))

    import commands
    loaded_commands = 'loaded commands: %s' % '|'.join(commands.__all__)

    if not name:
        if configuration.has_option('common', 'prg_name'):
            name = get_content(configuration.get('common', 'prg_name')).strip().split()[0]

    global parent_parser
    if name:
        parent_parser = argparse.ArgumentParser(prog=name, epilog=loaded_commands)
    else:
        parent_parser = argparse.ArgumentParser(epilog=loaded_commands)

    if not parent_parser:
        raise SystemError('No parser!')

    subparsers = parent_parser.add_subparsers(dest='command', help='Use -h|--help for more options')

    _subs = {}
    for _cmd in commands.__all__:
        if _cmd in _subs:
            continue

        parser = subparsers.add_parser(_cmd)
        _subs[_cmd] = parser

    _outs = []
    for name, value in configuration.items('options'):
        _out = _evaluate_option(configuration, name, value, parent_parser, subparsers, _subs)
        _outs.extend(_out)

    try:
        if new_args:
            args = parent_parser.parse_args(args=new_args)
        else:
            args = parent_parser.parse_args()

    except Exception as _e:
        _exit(1)

    if args.debug:
        for _out in _outs:
            log.view('.', _out)

    return args


def _evaluate_option(configuration, name, value, parser, subparser, subs):
    """ evaluate option helper """
    _outs = []
    _subs = []
    _added = []
    _section = 'option:{}'.format(name)
    _subparser = True if ('+' in name) else False
    if _subparser:
        _tmp = name.split('+')
        full_name = _tmp[0] + ':' + _tmp[1]
        # parent_parser = parser
        if _tmp[0] not in subs.keys():
            parser = subparser.add_parser(_tmp[0])
            subs[_tmp[0]] = parser
        else:
            parser = subs[_tmp[0]]

        name = _tmp[1]
        _section = 'option:{}'.format(full_name)

    if not get_bool(value):
        _outs.append('Ignored argument: {}'.format(name))
        return _outs

    _short = None
    if configuration.has_option(_section, 'short'):
        _short = configuration.get(_section, 'short').strip()

    _long = None
    if configuration.has_option(_section, 'long'):
        _long = configuration.get(_section, 'long').strip()

    _type = None
    if configuration.has_option(_section, 'type'):
        _type = configuration.get(_section, 'type').strip().lower()

    if not _type:
        _type = 'param'

    _required = False
    if configuration.has_option(_section, 'required'):
        _required = configuration.getboolean(_section, 'required')

    _default = ''
    if configuration.has_option(_section, 'default'):
        _default = configuration.get(_section, 'default').strip()

    _description = 'Option: {}'.format(name)
    if configuration.has_option(_section, 'description'):
        _description = configuration.get(_section, 'description').strip()

    if name not in _added:
        _added.append(name)
        _outs.append('Parsed argument: {}'.format(name))

    _param = None
    if _short and not _long:
        _param = _short
    elif _long and not _short:
        _param = _long

    if _type == 'bool':
        if _default:
            _default = get_bool(_default)

            if _param:
                parser.add_argument(
                    _param, action='store_true', default=_default, help=_description
                )
            else:
                parser.add_argument(
                    _short, _long, action='store_true', default=_default, help=_description
                )

            _outs.append('Added argument (1): {}'.format(name))

        else:
            if _param:
                parser.add_argument(
                    _param, action='store_true', help=_description
                )
            else:
                parser.add_argument(
                    _short, _long, action='store_true', help=_description
                )

            _outs.append('Added argument (2): {}'.format(name))

    elif _type == 'int':
        if _default:
            if _param:
                parser.add_argument(
                    _param, dest=name, type=int, required=_required, default=_default, help=_description
                )
            else:
                parser.add_argument(
                    _short, _long, dest=name, type=int, required=_required, default=_default, help=_description
                )

            _outs.append('Added argument (3): {}'.format(name))

        else:
            if _param:
                parser.add_argument(
                    _param, dest=name, type=int, required=_required, help=_description
                )
            else:
                parser.add_argument(
                    _short, _long, dest=name, type=int, required=_required, help=_description
                )

            _outs.append('Added argument (4): {}'.format(name))

    elif _type == 'param':
        if _default:
            if _param:
                if _param.startswith('-'):
                    parser.add_argument(
                        _param, dest=name, required=_required, default=_default, help=_description
                    )
                else:
                    if _required:
                        parser.add_argument(
                            _param, default=_default, help=_description
                        )
                    else:
                        parser.add_argument(
                            _param, default=_default, help=_description
                        )
            else:
                parser.add_argument(
                    _short, _long, dest=name, required=_required, default=_default, help=_description
                )

            _outs.append('Added argument (5): {}'.format(name))

        else:
            if _param:
                parser.add_argument(
                    _param, dest=name, required=_required, help=_description
                )
            else:
                parser.add_argument(
                    _short, _long, dest=name, required=_required, help=_description
                )

            _outs.append('Added argument (6): {}'.format(name))

    else:
        raise ValueError('{} - {} ({})'.format(messages.MSG_E_0001, 'name', str(name)))

    return _outs


def get_bool(value):
    """ get bool """
    return (value not in ['F', 'f', 'N', 'n', 'No', 'no', 'False', 'false', 'FALSE'])


def dump_json_string(data, indent=4, sort=True, byteify=False, debug=False):
    """ dump json string """
    # print('Data', data)
    if debug:
        log.view('>', 'Dump')

    _str = json.dumps(
        data,
        sort_keys=sort,
        indent=indent,
        separators=(', ', ' : '))
    return _str if not byteify else _str.encode()


def pprint(data):
    """
    Pretty prints json data.
    :type data: dict
    """
    print(dump_json_string(data, byteify=False))


def locate(patterns, root=os.curdir, recursive=True, debug=False):
    """ locate """
    folder = os.path.expanduser(root) if root.startswith('~') else root
    folder = os.path.abspath(folder)
    if debug:
        log.view('>', '[Debug] Folder: {}'.format(folder))

    if not os.path.exists(folder) or not os.path.isdir(folder):
        raise ValueError('{} ({})'.format(messages.MSG_E_0004, folder))

    if debug:
        log.view('>', '[Debug] patterns: {}'.format(patterns))

    for pattern in patterns:
        if debug:
            log.view('>', '[Debug] pattern: {}'.format(pattern))

        if recursive:
            for path, _, files in os.walk(folder, followlinks=True):
                for filename in fnmatch.filter(files, pattern):
                    yield os.path.join(path, filename)
        else:
            for filename in fnmatch.filter(os.listdir(folder), pattern):
                yield os.path.join(folder, filename)


def docopt_cmd(func):
    """
    This decorator is used to simplify the try/except block and pass the result
    of the docopt parsing to the called action.
    """
    def fn(self, arg):
        """ decorator """
        try:
            # print arg
            if '"' in arg:
                arg = arg.split('"')
                arg = [x.strip() for x in arg if x]
            elif "'" in arg:
                arg = arg.split("'")
                arg = [x.strip() for x in arg if x]

            # print arg
            opt = docopt(fn.__doc__, arg)

        except DocoptExit as e_:
            # The DocoptExit is thrown when the args do not match.
            # We print a message to the user and the usage block.

            # print('Invalid Usage! ({}{})'.format(fn.__name__.replace('do_', ''), ' {}'.format(arg) if arg else ''))
            print(e_)
            return ''

        except SystemExit as se_:
            # The SystemExit exception prints the usage for --help
            # We do not need to do the print here.
            if se_ and se_.args:
                print('Error1: %s' % se_)
            return ''

        return func(self, opt)

    fn.__name__ = func.__name__
    fn.__doc__ = func.__doc__
    fn.__dict__.update(func.__dict__)
    return fn
