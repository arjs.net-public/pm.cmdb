"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import sys
import datetime
import logging


logging.basicConfig(level=logging.INFO, stream=sys.stderr, format="%(message)s")
logger = logging.getLogger(__name__)

level1 = 0
fname = ''


def log(show=False):
    """
    Log what function is called
    :rtype : func
    :param show:
    """
    global level1

    def wrap(func):
        global level1

        def wrap_log(*args, **kwargs):
            global fname, level1

            fnameold = fname
            fname = func.__name__
            # logger = logging.getLogger(name)
            # logger.setLevel(logging.INFO)
            #logging.basicConfig(level=logging.INFO, stream=sys.stderr, format="%(message)s")
            #logger = logging.getLogger(__name__)
            logger.addHandler(logging.NullHandler())

            # add file handler
            #fh = logging.FileHandler("%s.log" % 'logging')
            fh = logging.NullHandler()
            fh.setLevel(logging.DEBUG)
            fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            formatter = logging.Formatter(fmt)
            fh.setFormatter(formatter)
            #ch.setFormatter(formatter)
            logger.addHandler(fh)
            #logger.addHandler(ch)

            level1 += 1
            sof = str(datetime.datetime.now())
            if len(sof) < 20:
                sof += ".000000"

            if len(sof) < 20:
                sof += ".000000"

            if show:
                logger.info('(%s) %s Entered "%s"' % (sof, '+' * level1, fname))
            ret = func(*args, **kwargs)
            eof = str(datetime.datetime.now())
            if len(sof) < 20:
                sof += ".000000"

            if len(sof) < 20:
                sof += ".000000"

            if show:
                logger.info('(%s) %s Left "%s"' % (eof, '+' * level1, fname))
            level1 -= 1
            fname = fnameold
            return func, ret

        return wrap_log

    return wrap


def info(visible, marker, name, text):
    """
    Simple info function
    :param visible:
    :param marker:
    :param name:
    :param text:
    :return: None
    """
    if not visible:
        return

    if name:
        logger.info('(%s) %s %s : %s' % (datetime.datetime.now(), marker * level1, name, text))
    else:
        logger.info('(%s) %s %s' % (datetime.datetime.now(), marker * level1, text))


def view(marker, text):
    """
    Simple view function
    :param marker:
    :param text:
    """
    if marker:
        m = marker * level1 if level1 > 0 else marker * 3
    else:
        m = None

    formatstring = '(%s) %s %s' % (datetime.datetime.now(), m, text) \
        if m else '(%s) %s' % (datetime.datetime.now(), text)

    logger.info(formatstring)
