# origin: https://gist.github.com/einnocent/8854896
# turn dict into an object that allows access to nested keys via dot notation
# from http://stackoverflow.com/questions/3797957/python-easily-access-deeply-nested-dict-get-and-set
# made three modifications:
#   --added `get()` method
#   --added `if not dict.__contains__...` to `__contains__()`
#   --can now accept None as key
# AS: 
# - Made Changes suggested by PyCharm
# - Renamed to DotDict, cause didn't liked the ify :-)
# - Changed the setdefault to dict signature
# - Removed a print key


class DotDict(dict):
    
    def __init__(self, value=None, **kwargs):
        super(DotDict, self).__init__(**kwargs)
        if value is None:
            pass
        elif isinstance(value, dict):
            for key in value:
                self.__setitem__(key, value[key])
        else:
            raise TypeError('expected dict')

    def __setitem__(self, key, value):
        if key is not None and '.' in key:
            my_key, rest_of_key = key.split('.', 1)
            target = self.setdefault(my_key, DotDict())
            if not isinstance(target, DotDict):
                raise KeyError('cannot set "%s" in "%s" (%s)' % (rest_of_key, my_key, repr(target)))

            target[rest_of_key] = value
        else:
            if isinstance(value, dict) and not isinstance(value, DotDict):
                value = DotDict(value)

            dict.__setitem__(self, key, value)

    def __getitem__(self, key):
        if key is None or '.' not in key:
            return dict.__getitem__(self, key)

        my_key, rest_of_key = key.split('.', 1)
        target = dict.__getitem__(self, my_key)
        if not isinstance(target, DotDict):
            raise KeyError('cannot get "%s" in "%s" (%s)' % (rest_of_key, my_key, repr(target)))

        return target[rest_of_key]

    def __delitem__(self, key):
        if key is None or '.' not in key:
            return dict.__delitem__(self, key)

        my_key, rest_of_key = key.split('.', 1)
        if rest_of_key is None:
            target = dict.__delitem__(self, my_key)
        else:
            target = dict.__getitem__(self, my_key)
            target.__delitem__(rest_of_key)

        if not isinstance(target, DotDict):
            raise KeyError('cannot get "%s" in "%s" (%s)' % (rest_of_key, my_key, repr(target)))

        return self

    def __contains__(self, key):
        if key is None or '.' not in key:
            return dict.__contains__(self, key)

        my_key, rest_of_key = key.split('.', 1)
        if not dict.__contains__(self, my_key):
            return False

        target = dict.__getitem__(self, my_key)
        if not isinstance(target, DotDict):
            return False

        return rest_of_key in target

    def setdefault(self, key, default=None):
        if key not in self:
            self[key] = default

        return self[key]

    def get(self, k, d=None):
        if DotDict.__contains__(self, k):
            return DotDict.__getitem__(self, k)

        return d

    __setattr__ = __setitem__
    __getattr__ = __getitem__
    __delattr__ = __delitem__
