"""
Logging module, with standard error messages, which can be extended with the [messages] section in common.config
"""
# imports
from .log import view

# some constants
__author__ = 'alexrjs'

# Messages
MSG_E_0001 = 'Argument was not specified!'
MSG_E_0002 = 'Specified file not found!'
MSG_E_0003 = 'Specified file is not a file!'
MSG_E_0004 = 'Specified folder is not a directory!'
MSG_E_0005 = 'No known action was specified!'
MSG_E_0006 = 'Specified action was not found or registered!'
MSG_E_0007 = 'Registered action has no function attached!'
MSG_E_0008 = 'Specified file already exists!'
MSG_E_0009 = 'No json data was loaded!'
MSG_E_0010 = 'No user specified!'
MSG_E_0011 = 'No password specified!'
MSG_E_0012 = 'Specified folder still exists!'

# messages
MESSAGES = {
    'MSG_E_0001': MSG_E_0001,
    'MSG_E_0002': MSG_E_0002,
    'MSG_E_0003': MSG_E_0003,
    'MSG_E_0004': MSG_E_0004,
    'MSG_E_0005': MSG_E_0005,
    'MSG_E_0006': MSG_E_0006,
    'MSG_E_0007': MSG_E_0007,
    'MSG_E_0008': MSG_E_0008,
    'MSG_E_0009': MSG_E_0009,
    'MSG_E_0010': MSG_E_0010,
    'MSG_E_0011': MSG_E_0011,
    'MSG_E_0012': MSG_E_0012,
}


def messages_extend(msgs, debug=False):
    if not msgs:
        if debug:
            view('>', '[Debug] Empty messages argument')

        return

    if not isinstance(msgs, list):
        if debug:
            view('>', '[Debug] Not a tuple list')

        return

    for msg_no, msg_text in msgs:
        u_msg = msg_no.upper()
        if u_msg not in MESSAGES:
            MESSAGES[u_msg] = msg_text
            if debug:
                view('>', '[Debug] Added message: {} = {}'.format(u_msg, msg_text))

        elif debug:
            view('>', '[Debug] Message already there: {}'.format(u_msg))

    return
