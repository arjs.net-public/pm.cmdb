"""
Entry point for the main program.
"""
import os
import common

# some constants
__author__ = 'alexrjs'

# some variables
specific_configuration = None


if __name__ == '__main__':
    _config_file = os.path.realpath(os.path.join(os.path.dirname(__file__), 'default.config'))
    if _config_file and os.path.exists(_config_file):
        common.configuration = common.fetch_configuration(_config_file)
    else:
        raise EnvironmentError('{} ({})'.format(common.messages.MSG_E_0002, _config_file))

    args = common.fetch_arguments(common.configuration)
    common.messages_extend(common.configuration.items('messages'), args.debug)
    common.check_options(args, common.configuration)
    common.run(args, common.configuration)
