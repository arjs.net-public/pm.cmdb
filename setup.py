#!/usr/bin/env python
import os
from glob import glob
from os.path import basename, splitext
from setuptools import setup, find_packages


def read(filename):
    """
    Just read a text file with information
    """
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

setup(
    name=read('config/name.txt'),
    version=read('config/version.txt'),
    author='Alex Schiessl',
    author_email='alexrjs@gmail.com',
    url='http://arjs.net',
    license='Unlicense, http://unlicense.org',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    # trying to add files...
    include_package_data=True,
    package_data={
        # If any package contains *.txt files, include them:
        # And include any *.dat files found in the 'data' subdirectory
        # of the 'mypkg' package, also:
        '': ['../*.md', '../LICENSE', 'config/*.txt', 'data/*.*']
    },
    install_requires=[],
    description=read('config/short.txt'),
    long_description=read('config/description.txt'),
    scripts=['src/bin/cmdb']
)
